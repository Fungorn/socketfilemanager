package com.fungorn.Server;

abstract class AbstractHandler extends Thread {
    abstract void getFileList();
    abstract void getFileList(String filename);
    abstract void getFileContent(String filename);
}
