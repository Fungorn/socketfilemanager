package com.fungorn.Server;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClientHandler extends AbstractHandler {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    private File currentDirectory;
    private String request;
    private String response;

    ClientHandler(Socket socket) {
        this.socket = socket;
        currentDirectory = Server.INSTANCE.getRoot();

    }

    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            do {
                try {
                    request = in.readLine();
                } catch (SocketException e) {
                    if (!socket.isConnected() || !in.ready())
                        System.err.println("Connection interrupted.");
                    else
                        e.printStackTrace();
                }
                System.out.println(request);

                if (request.equals(".")) {
                    currentDirectory = Server.INSTANCE.getRoot();
                    getFileList();
                }  else if (request.equals("..")) {
                    currentDirectory = currentDirectory.getParentFile();
                    getFileList();
                } else if (request.contains(".txt")) {
                    getFileContent(request);
                } else if (!request.isEmpty()) {
                    getFileList(request);
                } else {
                    response = "Invalid request format.\n";
                }
                out.print(response);
                out.flush();
            } while (socket.isConnected());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    void getFileList() {
        if (currentDirectory.listFiles() != null)
            response = Stream.of(currentDirectory.listFiles())
                    .map(File::getName)
                    .collect(Collectors.toList()) + "\n";
            //response = Arrays.toString(currentDirectory.listFiles()) + '\n';
    }

    @Override
    void getFileList(String filename) {
        if (currentDirectory.listFiles() != null) {
            currentDirectory = Arrays.stream(currentDirectory.listFiles())
                    .filter(file -> (file.getName().equals(filename)))
                    .findAny()
                    .orElse(currentDirectory);
        }
        getFileList();
    }

    @Override
    void getFileContent(String filename) {
        if (currentDirectory.listFiles() != null) {
            Arrays.stream(currentDirectory.listFiles())
                    .filter(f -> (f.getName().equals(filename)))
                    .findAny()
                    .ifPresent(res -> {
                if (res.isFile() && res.canRead()) {
                    try (BufferedReader br = new BufferedReader(new FileReader(res))) {
                        StringBuilder sb = new StringBuilder();
                        String line = br.readLine();

                        while (line != null) {
                            sb.append(line);
                            sb.append(System.lineSeparator());
                            line = br.readLine();
                        }
                        response = sb.toString() + '\n';
                    } catch (FileNotFoundException e) {
                        //e.printStackTrace();
                        System.err.println(Calendar.getInstance().getTime() + "No such file.");
                    } catch (IOException e) {
                        //e.printStackTrace();
                        System.err.println(Calendar.getInstance().getTime() + "Error while reading from file.");
                    }
                }
            });
        }
    }
}
