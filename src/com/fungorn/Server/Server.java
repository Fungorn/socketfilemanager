package com.fungorn.Server;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public enum Server {
    INSTANCE;

    private static final int PORT = 8005;

    private final File root = new File("/");

    public File getRoot() {
        return root;
    }

    public void start() {
        System.out.println("Initializing server socket at port " + PORT);

        ServerSocket serverSocket;
        Socket socket;

        try {
            serverSocket = new ServerSocket(PORT);
            while (true) {
                socket = serverSocket.accept();
                // new thread for a client
                new ClientHandler(socket).start();
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
