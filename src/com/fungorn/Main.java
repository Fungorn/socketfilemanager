package com.fungorn;

import com.fungorn.Server.Server;

public class Main {

    public static void main(String[] args) {
        Server server = Server.INSTANCE;
        server.start();
    }
}
